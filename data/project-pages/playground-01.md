---
id: 'playground-01'
title: 'Gimbalabs Playground'
date: '2021-09-03'
tags: ['playground', 'in progress']
author: 'James Dunseith'
budget: 0
---      

## Gimbalabs Playground

- Gimbalabs Playground meets every Tuesday at 4pm UTC
- Live in Discord
- All are welcome to present
- Link to Miro board