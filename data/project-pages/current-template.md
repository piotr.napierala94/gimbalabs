---
id: 'template'
title: 'Project Template'
date: '2021-03-05'
tags: ['initiative', 'playground', 'help wanted', 'in progress', 'complete']
author: 'James Dunseith'
budget: 100
---      

# Project Template

This is a sample project. You can add anything you'd like in Markdown. Options for flexibility beyond Markdown coming soon.

## What to include:
1. Video of recent meeting
2. How to get involved, how to start - via Discord, via participation, via contribution
3. Link to GitLab, Miro -- OSS everything!

Should learn, tinker, participate be tags?

Figure out how to embed html - read remark docs
<iframe width="768" height="432" src="https://miro.com/app/live-embed/o9J_lOS6rkY=/?moveToViewport=-6909,-2365,11603,8031" frameBorder="0" scrolling="no" allowFullScreen></iframe>